<!DOCTYPE html>
<html lang="en">
<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="favicon.png" type="image/png">

        <title>Login&#8482; .:. Alpha.md</title>

        <link rel="stylesheet" href="css/font-awesome.min.css">
        <link rel="stylesheet" href="css/style_login.css">


    </head>

    <body class="signwrapper">

        <div class="sign-overlay"></div>
        <div class="signpanel"></div>
        <div class="panel signin">
            <center><img src="img/logo2.png" alt="Alpha"></center><hr>
            <div class="panel-heading">
                <h1>Login Page&#8482;</h1>
                        <h4 class="panel-title">Welcome! Please signin.</h4>
                </div>
            <div class="panel-body">

                <form action="" method="post">
                    <div class="form-group mb10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                            <input type="text" class="form-control" placeholder="Email Address" name="email">
                        </div>
                    </div>
                    <div class="form-group nomargin">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                            <input type="password" class="form-control" placeholder="Password" name="pword">
                        </div>
                    </div>
                    <div><a href="#" class="forgot">Forgot password?</a></div>
                    <div class="form-group">
                        <button class="btn btn-success btn-quirk btn-block" type="submit">Sign In</button>
                    </div>
                </form>
                <hr class="oinvisible">
                <center><a href="./?rdr=home">&laquo; Back Home</a></center>
            </div>
        </div><!-- panel -->

    </body>

    
</html>
