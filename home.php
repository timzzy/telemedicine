<?php 
      
      $page="home";
      $pagetitle="Home - Alpha.md | Patient ";
      include_once('pages/header.php');
?>


         <div id='main' class='all_colors' data-scroll-offset='0'>
            
        <?php include_once('pages/headerslider.php');?>

            <div id='operatore' class='avia-section main_color avia-section-default avia-no-shadow avia-bg-style-scroll  avia-builder-el-44  el_after_av_tab_section  el_before_av_image_hotspot   container_wrap fullsize' style = 'background-color: #f8f8f8; background-image: '  >
               <div class='container' >
                  <div class='template-page content  av-content-full alpha units'>
                     <div class='post-entry post-entry-type-page post-entry-3439'>
                        <div class='entry-content-wrapper clearfix'>
                           <div style='padding-bottom:10px; font-size:38px;' class='av-special-heading av-special-heading-h2  blockquote modern-quote modern-centered  avia-builder-el-45  avia-builder-el-no-sibling  av-inherit-size '>
                              <h2 class='av-special-heading-tag '  itemprop="headline">Procedure as a patient</h2>
                              <div class='special-heading-border'>
                                 <div class='special-heading-inner-border' ></div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- close content main div -->
               </div>
            </div>
            <div  class='main_color av-fullwidth-hotspots  avia-builder-el-46  el_after_av_section  el_before_av_section   container_wrap fullsize'   >
               <div class='av-hotspot-image-container avia_animate_when_almost_visible   av-hotspot-numbered av-mobile-fallback-active    avia-builder-el-46  el_after_av_section  el_before_av_section  '  itemprop="ImageObject" itemscope="itemscope" itemtype=""  >
                  <div class='av-hotspot-container'>
                     <div class='av-hotspot-container-inner-cell'>
                        <div class='av-hotspot-container-inner-wrap'>
                           <div class='av-image-hotspot' data-avia-tooltip-position='left' data-avia-tooltip-alignment='bottom' data-avia-tooltip-class='av-tt-default-width av-tt-pos-left av-tt-align-bottom  av-mobile-fallback-active  main_color av-tt-hotspot' data-avia-tooltip='&lt;p&gt;Insert User and Password of the caregiver.&lt;/p&gt;
                              ' style='top: 81.4%; left: 17%; '>
                              <div class='av-image-hotspot_inner' style='background-color: #5fb4e4; color: #ffffff;'>1</div>
                              <div class='av-image-hotspot-pulse' style='background-color:#0b3a66;'></div>
                           </div>
                           <div class='av-image-hotspot' data-avia-tooltip-position='top' data-avia-tooltip-alignment='left' data-avia-tooltip-class='av-tt-default-width av-tt-pos-above av-tt-align-left  av-mobile-fallback-active  main_color av-tt-hotspot' data-avia-tooltip='&lt;p&gt;Search a patient already registered or input new patient.&lt;/p&gt;
                              ' style='top: 14%; left: 77.7%; '>
                              <div class='av-image-hotspot_inner' style='background-color: #5fb4e4; color: #ffffff;'>2</div>
                              <div class='av-image-hotspot-pulse' style='background-color:#0b3a66;'></div>
                           </div>
                           <div class='av-image-hotspot' data-avia-tooltip-position='left' data-avia-tooltip-alignment='top' data-avia-tooltip-class='av-tt-default-width av-tt-pos-left av-tt-align-top  av-mobile-fallback-active  main_color av-tt-hotspot' data-avia-tooltip='&lt;p&gt;Select the doctor available for the visit.&lt;/p&gt;
                              ' style='top: 28.7%; left: 12.7%; '>
                              <div class='av-image-hotspot_inner' style='background-color: #5fb4e4; color: #ffffff;'>3</div>
                              <div class='av-image-hotspot-pulse' style='background-color:#0b3a66;'></div>
                           </div>
                           <div class='av-image-hotspot' data-avia-tooltip-position='right' data-avia-tooltip-alignment='bottom' data-avia-tooltip-class='av-tt-default-width av-tt-pos-right av-tt-align-bottom  av-mobile-fallback-active  main_color av-tt-hotspot' data-avia-tooltip='&lt;p&gt;Streaming video with the doctor&lt;/p&gt;
                              ' style='top: 77.1%; left: 84.1%; '>
                              <div class='av-image-hotspot_inner' style='background-color: #5fb4e4; color: #ffffff;'>4</div>
                              <div class='av-image-hotspot-pulse' style='background-color:#0b3a66;'></div>
                           </div>
                           <div class='av-image-hotspot' data-avia-tooltip-position='bottom' data-avia-tooltip-alignment='centered' data-avia-tooltip-class='av-tt-default-width av-tt-pos-below av-tt-align-centered  av-mobile-fallback-active  main_color av-tt-hotspot' data-avia-tooltip='&lt;p&gt;Select the device for the exam. Results will be transmitted directly to the doctor.&lt;/p&gt;
                              ' style='top: 69.1%; left: 45.6%; '>
                              <div class='av-image-hotspot_inner' style='background-color: #5fb4e4; color: #ffffff;'>5</div>
                              <div class='av-image-hotspot-pulse' style='background-color:#0b3a66;'></div>
                           </div>
                           <img class='avia_image ' src='img/software-operator.png' alt='' title='software-operatore'  height="832"width="1920"  itemprop="thumbnailUrl"  />
                        </div>
                     </div>
                  </div>
                  <div class='av-hotspot-fallback-tooltip'>
                     <div class='av-hotspot-fallback-tooltip-count'>
                        1
                        <div class='avia-arrow'></div>
                     </div>
                     <div class='av-hotspot-fallback-tooltip-inner clearfix'>
                        <p>
                           Insert User and Password of the caregiver.
                        </p>
                     </div>
                  </div>
                  <div class='av-hotspot-fallback-tooltip'>
                     <div class='av-hotspot-fallback-tooltip-count'>
                        2
                        <div class='avia-arrow'></div>
                     </div>
                     <div class='av-hotspot-fallback-tooltip-inner clearfix'>
                        <p>
                           Search a patient already registered or input new patient.
                        </p>
                     </div>
                  </div>
                  <div class='av-hotspot-fallback-tooltip'>
                     <div class='av-hotspot-fallback-tooltip-count'>
                        3
                        <div class='avia-arrow'></div>
                     </div>
                     <div class='av-hotspot-fallback-tooltip-inner clearfix'>
                        <p>
                           Select the doctor available for the visit.
                        </p>
                     </div>
                  </div>
                  <div class='av-hotspot-fallback-tooltip'>
                     <div class='av-hotspot-fallback-tooltip-count'>
                        4
                        <div class='avia-arrow'></div>
                     </div>
                     <div class='av-hotspot-fallback-tooltip-inner clearfix'>
                        <p>
                           Streaming video with the doctor
                        </p>
                     </div>
                  </div>
                  <div class='av-hotspot-fallback-tooltip'>
                     <div class='av-hotspot-fallback-tooltip-count'>
                        5
                        <div class='avia-arrow'></div>
                     </div>
                     <div class='av-hotspot-fallback-tooltip-inner clearfix'>
                        <p>
                           Select the device for the exam. Results will be transmitted directly to the doctor.
                        </p>
                     </div>
                  </div>
               </div>
            </div>
          


            <div  class='main_color av-fullwidth-hotspots  avia-builder-el-50  el_after_av_section  el_before_av_section   container_wrap fullsize'   >
               <div class='av-hotspot-image-container avia_animate_when_almost_visible   av-hotspot-numbered av-mobile-fallback-active    avia-builder-el-50  el_after_av_section  el_before_av_section  '  itemprop="ImageObject" itemscope="itemscope" itemtype=""  >
                  <div class='av-hotspot-container'>
                     <div class='av-hotspot-container-inner-cell'>
                        <div class='av-hotspot-container-inner-wrap'>
                          
               </div>
            </div>
            <div id='oav-layout-grid-1' class=''   >
               <div class="flex_cell no_margin av_one_half  avia-builder-el-65  el_before_av_cell_one_half  avia-builder-el-first   avia-full-stretch av-zero-padding " style='background:url(img/infermiera-multidoctor.jpg) center center no-repeat scroll; vertical-align:middle; padding:0px; background-color:#ffffff; '>
                  <div class='flex_cell_inner' >
                     <section class="oav_textblock_section "  itemscope="itemscope" itemtype="" >
                        <div class='oavia_textblock  '   itemprop="text" >
                           <p style="text-align: center;">
                           <div class='avia-image-container  av-styling- avia-builder-el-67  avgia-builder-el-no-sibling   avia-align-center '  itemprop="oImageObject" itemscope="itemsgcope" itemtype=""  >
                              <div class='avia-image-container-inner'>
                                <img class='avia_imadge ' src='img/splitscreen.jpg' alt='' title=''    />
                              </div>
                           </div>
                           </p>
                           
                        </div>
                     </section>
                  </div>
               </div>
               <div class="flex_cell no_margin av_one_half  avia-builder-el-68  el_after_av_cell_one_half  avia-builder-el-last" style='vertical-align:middle; padding:80px; background-color:#002136; '>
                  <div class='flex_cell_inner' >
                     <div style='padding-bottom:25px; color:#ffffff;font-size:34px;' class='av-special-heading av-special-heading-h3 custom-color-heading blockquote modern-quote  avia-builder-el-69  el_before_av_contact  avia-builder-el-first   av-inherit-size '>
                        <div class ='av-subheading av-subheading_above av_custom_color ' style='font-size:15px;'>
                           <p>We Will like to hear from you</p>
                        </div>
                        <h3 class='av-special-heading-tag '  itemprop="headline"  >Contact Us</h3>
                        <div class='special-heading-border'>
                           <div class='special-heading-inner-border' style='border-color:#ffffff'></div>
                        </div>
                     </div>
                     <form action="NILLsoftware/" method="post"  class="avia_ajax_form av-form-labels-visible   avia-builder-el-70  el_after_av_heading  avia-builder-el-last  av-custom-form-color av-light-form " data-avia-form-id="1" data-avia-redirect='NILLthanks/'>
                        <fieldset>
                           <p class=' first_form  form_element form_element_third' id='element_avia_1_1'><label for="avia_1_1">Name <abbr class="required" title="required">*</abbr></label> <input name="avia_1_1" class="text_input is_empty" type="text" id="avia_1_1" value="" /></p>
                           <p class=' form_element form_element_third' id='element_avia_2_1'><label for="avia_2_1">E-Mail <abbr class="required" title="required">*</abbr></label> <input name="avia_2_1" class="text_input is_email" type="text" id="avia_2_1" value="" /></p>
                           <p class=' form_element form_element_third' id='element_avia_3_1'><label for="avia_3_1">Telephone Number <abbr class="required" title="required">*</abbr></label> <input name="avia_3_1" class="text_input is_empty" type="text" id="avia_3_1" value="" /></p>
                           <p class=' first_form  form_element form_fullwidth av-last-visible-form-element' id='element_avia_4_1'>	 <label for="avia_4_1" class="textare_label hidden textare_label_avia_4_1">Message <abbr class="required" title="required">*</abbr></label>	 <textarea  name="avia_4_1" class="text_area is_empty" cols="40" rows="7" id="avia_4_1" ></textarea></p>
                           <p class="hidden"><input type="text" name="avia_5_1" class="hidden " id="avia_5_1" value="" /></p>
                           <p class="form_element "><input type="hidden" value="1" name="avia_generated_form1" /><input type="submit" value="Send Message" class="button"  data-sending-label="Sending"/></p>
                        </fieldset>
                     </form>
                     <div id="ajaxresponse_1" class="ajaxresponse ajaxresponse_1 hidden"></div>
                  </div>
               </div>
            </div>
            
            <?php include_once('pages/footer.php');?>


           
            <!-- end main -->
         </div>
         <!-- end wrap_all -->
      </div>
      <a href='#top' title='Scroll to top' id='scroll-top-link' aria-hidden='true' data-av_icon='' data-av_iconfont='entypo-fontello'><img src="img/top.png"> <span class="avia_hidden_link_text">Scroll to top</span></a>
      
   </body>
</html>

