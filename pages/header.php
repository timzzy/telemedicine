

<!DOCTYPE html>
<html lang="en-US" prefix="og: http://ogp.me/ns#" class="html_stretched responsive av-preloader-disabled av-default-lightbox  html_header_top html_logo_left html_main_nav_header html_menu_right html_slim html_header_sticky_disabled html_header_shrinking_disabled html_header_topbar_active html_mobile_menu_phone html_header_searchicon_disabled html_content_align_center html_header_unstick_top_disabled html_header_stretch_disabled html_av-overlay-side html_av-overlay-side-classic html_av-submenu-noclone html_entry_id_3439 av-no-preview html_text_menu_active ">
   
   <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
   
   <head>

      <meta charset="UTF-8" />

      <!-- mobile setting -->
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

      <title><?php echo $pagetitle;?></title>

              
      <link rel='stylesheet' id='avia-merged-styles-css'  href='css/style.css' type='text/css' media='all' />
      <link rel='stylesheet' id='avia-merged-styles-css'  href='css/style_b.css' type='text/css' media='all' />
      <link rel='stylesheet' id='avia-merged-styles-css'  href='css/thecss.css' type='text/css' media='all' />
      <link rel='stylesheet' id='avia-merged-styles-css'  href='css/bootstrap.min.css' type='text/css' media='all' />
      <script type='text/javascript' src='js/jquery_a.js?ver=1.12.4'></script>
      <script type='text/javascript' src='js/jquery_b.js'></script>
      
      <link rel="icon" href="">
      <meta name="generator" content="A client Application to communincate with a doctor" />

      <!-- To speed up the rendering and to display the site as fast as possible to the user we include some styles and scripts for above the fold content inline -->
      <script type="text/javascript">'use strict';var avia_is_mobile=!1;if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)&&'ontouchstart' in document.documentElement){avia_is_mobile=!0;document.documentElement.className+=' avia_mobile '}
         else{document.documentElement.className+=' avia_desktop '};document.documentElement.className+=' js_active ';(function(){var e=['-webkit-','-moz-','-ms-',''],n='';for(var t in e){if(e[t]+'transform' in document.documentElement.style){document.documentElement.className+=' avia_transform ';n=e[t]+'transform'};if(e[t]+'perspective' in document.documentElement.style)document.documentElement.className+=' avia_transform3d '};if(typeof document.getElementsByClassName=='function'&&typeof document.documentElement.getBoundingClientRect=='function'&&avia_is_mobile==!1){if(n&&window.innerHeight>0){setTimeout(function(){var e=0,o={},a=0,t=document.getElementsByClassName('av-parallax'),i=window.pageYOffset||document.documentElement.scrollTop;for(e=0;e<t.length;e++){t[e].style.top='0px';o=t[e].getBoundingClientRect();a=Math.ceil((window.innerHeight+i-o.top)*0.3);t[e].style[n]='translate(0px, '+a+'px)';t[e].style.top='auto';t[e].className+=' enabled-parallax '}},50)}}})();
      </script>
      
     
   </head>
   <body id="top" class="page-template-default page page-id-3439 stretched raleway open_sans " itemscope="itemscope" itemtype="https://schema.org/WebPage" >
      
      <div id='wrap_all'>
        
         <header id='header' class='all_colors header_color light_bg_color  av_header_top av_logo_left av_main_nav_header av_menu_right av_slim av_header_sticky_disabled av_header_shrinking_disabled av_header_stretch_disabled av_mobile_menu_phone av_header_searchicon_disabled av_header_unstick_top_disabled av_bottom_nav_disabled  av_alternate_logo_active av_header_border_disabled'  role="banner" itemscope="itemscope" itemtype="https://schema.org/WPHeader" >
            <div id='header_meta' class='container_wrap container_wrap_meta  av_phone_active_right av_extra_header_active av_entry_id_3439'>
               <div class='container'>
                  <nav class='sub_menu'  role="navigation" itemscope="itemscope" itemtype="https://schema.org/SiteNavigationElement" >
                     <ul class='avia_wpml_language_switch avia_wpml_language_switch_extra'>
                        <li class='language_en avia_current_lang'><a href='index.html'>   <span class='language_flag'> <!--<img title='English' src='' alt='English' /> --></span>  <span class='language_native'>English</span> <span class='language_translated'>English</span>   <span class='language_code'>en</span></a></li>
                        <li class='language_it '><a href=''>   <span class='language_flag'><img title='Available in Africa' src='img/it.png' alt='Alphha.md' /></span>  <span class='language_native'>Nigeria</span> <span class='language_translated'>Nigeria</span>   <span class='language_code'>NG</span></a></li>
                     </ul>
                  </nav>
                  <div class='phone-info '><span>Call us:: +234 0000 0000 0000 for more info</span></div>
               </div>
            </div>
            <div  id='header_main' class='container_wrap container_wrap_logo'>
               <div class='container av-logo-container'>
                  <div class='inner-container'>
                     <span class='logo'><a href='NILL'><img height='100' width='300' src='img/logo.png' alt='Remote Healthcare System' /></a></span>
                     <nav class='main_menu' data-selectname='Select a page'  role="navigation" itemscope="itemscope" itemtype="https://schema.org/SiteNavigationElement" >
                        <div class="avia-menu av-main-nav-wrap">
                           <ul id="avia-menu" class="menu av-main-nav">
                              <!--<li id="menu-item-3821" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-top-level menu-item-top-level-1"><a href="./?rdr=signup" itemprop="url"><span class="avia-bullet"></span><span class="avia-menu-text">Sign Up</span><span class="avia-menu-fx"><span class="avia-arrow-wrap"><span class="avia-arrow"></span></span></span></a></li>
                              <li id="menu-item-3821" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-top-level menu-item-top-level-1"><a href="./?rdr=login" itemprop="url"><span class="avia-bullet"></span><span class="avia-menu-text">Login</span><span class="avia-menu-fx"><span class="avia-arrow-wrap"><span class="avia-arrow"></span></span></span></a></li>
                              <li id="menu-item-2000" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-top-level menu-item-top-level-2">
                                 <a href="#" itemprop="url"><span class="avia-bullet"></span><span class="avia-menu-text">Login</span><span class="avia-menu-fx"><span class="avia-arrow-wrap"><span class="avia-arrow"></span></span></span></a>
                                 <ul class="sub-menu">
                                    <li id="menu-item-2002" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="NILLtelemedicine-company/" itemprop="url"><span class="avia-bullet"></span><span class="avia-menu-text">Farmaforniture S.R.L.</span></a></li>
                                    <li id="menu-item-2031" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="NILLpartners/" itemprop="url"><span class="avia-bullet"></span><span class="avia-menu-text">Partners</span></a></li>
                                    <li id="menu-item-2089" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="NILLdistributors/" itemprop="url"><span class="avia-bullet"></span><span class="avia-menu-text">Distributors</span></a></li>
                                    <li id="menu-item-2317" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="NILLcompany-profile/" itemprop="url"><span class="avia-bullet"></span><span class="avia-menu-text">Company Profile</span></a></li>
                                 </ul>
                              </li>-->
                               <?php
                                    if (isset($page) AND ($page)!="home") {
                               ?>  
                              <li id="menu-item-4284" class="menu-item menu-item-type-custom menu-item-object-custom av-menu-button av-menu-button-colored menu-item-top-level menu-item-top-level-7"><a target="" href="./?rdr=home" itemprop="url"><span class="avia-bullet"></span><span class="avia-menu-text">Home</span><span class="avia-menu-fx"><span class="avia-arrow-wrap"><span class="avia-arrow"></span></span></span></a></li>
                              <?php
                                   }//Endds:: if page isNot Home
                               ?>  

                              <li id="menu-item-4284" class="menu-item menu-item-type-custom menu-item-object-custom av-menu-button av-menu-button-colored menu-item-top-level menu-item-top-level-7"><a target="" href="./?rdr=login" itemprop="url"><span class="avia-bullet"></span><span class="avia-menu-text">Login</span><span class="avia-menu-fx"><span class="avia-arrow-wrap"><span class="avia-arrow"></span></span></span></a></li>
                              <li id="menu-item-4284" class="menu-item menu-item-type-custom menu-item-object-custom av-menu-button av-menu-button-colored menu-item-top-level menu-item-top-level-7"><a target="" href="" itemprop="url"><span class="avia-bullet"></span><span class="avia-menu-text">Sign Up</span><span class="avia-menu-fx"><span class="avia-arrow-wrap"><span class="avia-arrow"></span></span></span></a></li>
                              <li id="menu-item-4284" class="menu-item menu-item-type-custom menu-item-object-custom av-menu-button av-menu-button-colored menu-item-top-level menu-item-top-level-7"><a target="" href="./?rdr=home" itemprop="url"><span class="avia-bullet"></span><span class="avia-menu-text">View Doctor's List</span><span class="avia-menu-fx"><span class="avia-arrow-wrap"><span class="avia-arrow"></span></span></span></a></li>
                              <li class="av-burger-menu-main menu-item-avia-special ">
                                 <a href="#">
                                 <span class="av-hamburger av-hamburger--spin av-js-hamburger">
                                 <span class="av-hamburger-box">
                                 <span class="av-hamburger-inner"></span>
                                 <strong>Menu</strong>
                                 </span>
                                 </span>
                                 </a>
                              </li>
                           </ul>
                        </div>
                     </nav>
                  </div>
               </div>
               <!-- end container_wrap-->
            </div>
            <div class='header_bg'></div>
            <!-- end header -->
         </header>